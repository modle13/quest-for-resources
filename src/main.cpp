#include "command-handler.h"
#include "dungeon.h"
#include "player.h"
#include "prompt.h"
#include <string>

using namespace std;

Dungeon dungeon;
Player* player;
COMMAND_HANDLER_H::CommandHandler commandHandler;

void printIntro() {
  PROMPT_H::report("\nWelcome, " + player->getName() + " of " + player->getOrigin());
  PROMPT_H::report("\nYou are in a damp cave");
}

void printDetails() {
  string details = "\nBloody marks on the wall read: " +
      dungeon.getRoomName(player->getXPosition(), player->getYPosition()) +
      " chamber\n";
  PROMPT_H::report(details);
  string position = "position: (" + to_string(player->getXPosition()) + "," + to_string(player->getYPosition()) + ")";
  PROMPT_H::report(position);
}

int main() {
  player = new Player(dungeon);

  printIntro();

  string s;
  do {
    printDetails();

    PROMPT_H::reportInline("> ");
    s = PROMPT_H::getLine();

    commandHandler.processCommand(s, player);
  }
  while (s != "quit");

  return 0;
}
