#ifndef PROMPT_H
#define PROMPT_H
#include <string>

using namespace std;

string prompt(string msg);

void reportInline(string out);

void report(string out);

string getLine();

#endif
