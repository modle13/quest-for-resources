#ifndef DUNGEON_H
#define DUNGEON_H
#include <string>

#include <unordered_map>

using namespace std;

struct Direction {
  int x;
  int y;
};

class Dungeon {
  private:
    string caveMap[9][9] = {
      {"room00","room10","room20","room30","room40","room50","room60","room70","room80"},
      {"room01","room11","room21","room31","room41","room51","room61","room71","room81"}
    };
    unordered_map<string, Direction> directions;
  public:
    Dungeon() {
      // if multiple dungeons, this will need to change
      // right now, Dungeon should be singleton
      // how to do that?
      init();
    };
    void init() {
      Direction north = {.x = 0, .y = -1};
      Direction south = {.x = 0, .y = 1};
      Direction east = {.x = 1, .y = 0};
      Direction west = {.x = -1, .y = 0};

      directions["north"] = north;
      directions["south"] = south;
      directions["east"] = east;
      directions["west"] = west;
    }
    Direction getDirection(string _direction);
    Direction getCoordinates(int x, int y, string _direction);
    string getRoomName(int xPos, int yPos);
    bool moveAllowed(int xPos, int yPox, string direction);
};

#endif
